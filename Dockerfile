ARG PHP_VERSION
ARG COMPOSER_VERSION

FROM composer:${COMPOSER_VERSION} as composer
FROM php:${PHP_VERSION}-cli

ARG WITH_XDEBUG

ENV PROJECT_NAME='magento'
ENV PHP_IDE_CONFIG='serverName=${PROJECT_NAME}'

RUN set -eux; \
      DEBIAN_FRONTEND=noninteractive apt-get update -y -qq --fix-missing; \
      DEBIAN_FRONTEND=noninteractive apt-get upgrade -y -qq; \
      DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends git zip unzip ssh-client; \
      DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends libxml2-dev libxslt-dev libzip-dev libyaml-dev libonig-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev;

RUN set -eux; \
      docker-php-ext-configure zip; \
      docker-php-ext-configure gd --with-jpeg --with-freetype; \
      docker-php-ext-install -j$(nproc) gd shmop dom intl mbstring pdo_mysql xsl zip soap bcmath sockets pcntl; \
      pecl install -fo redis yaml; \
      docker-php-ext-enable redis yaml;

RUN set -eux; \
      DEBIAN_FRONTEND=noninteractive apt-get remove -y libxml2-dev libxslt1-dev libzip-dev libyaml-dev libonig-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev; \
      DEBIAN_FRONTEND=noninteractive apt-get clean -y; \
      rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*; \
      mkdir -p /var/www/magento && chown -R www-data: /var/www/magento;

RUN set -eux; \
      if [ "${WITH_XDEBUG}" = 'true' ] || [ "${WITH_XDEBUG}" = '1' ] || [ "${WITH_XDEBUG}" = 'yes' ]; then \
        pecl install -fo xdebug; \
        docker-php-ext-enable xdebug; \
        echo '; Xdebug settings will only kick in if the Xdebug module is loaded' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo '' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo 'xdebug.mode=debug' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo 'xdebug.start_with_request=yes' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo 'xdebug.client_port=9003' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo 'xdebug.client_host=host.docker.internal' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo 'xdebug.discover_client_host=false' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
        echo 'xdebug.idekey=PHPSTORM' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini; \
      fi;

RUN set -eux; \
      echo 'memory_limit = 2G' >> /usr/local/etc/php/conf.d/docker-php-magento.ini; \
      echo 'upload_max_filesize = 8M' >> /usr/local/etc/php/conf.d/docker-php-magento.ini; \
      echo 'post_max_size = 8M' >> /usr/local/etc/php/conf.d/docker-php-magento.ini; \
      echo 'max_execution_time = 1800' >> /usr/local/etc/php/conf.d/docker-php-magento.ini;

# Install Composer globally
ARG COMPOSER_VERSION
ENV COMPOSER_ALLOW_SUPERUSER 1
COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /var/www/magento

RUN ["chmod", "-R", "+x", "/usr/local/bin/"]

CMD ["php", "-a"]

USER www-data
